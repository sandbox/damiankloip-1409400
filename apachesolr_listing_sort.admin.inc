<?php

/**
 * Form callback for global settings page/tab.
 */
function apachesolr_listing_sort_settings_form() {
  $form = array();

  $fields = apachesolr_listing_sort_available_fields();
  $options = _apachesolr_listing_sort_format_options_list($fields);

  $form['global'] = array(
    '#type' => 'fieldset',
    '#title' => t('Global defaults'),
    '#description' => t('These default options will be applied to all apache solr listings that have not been overridden.'),
  );
  $form['global']['apachesolr_listing_sort_default_field'] = array(
    '#type' => 'select',
    '#title' => t('Field to sort by'),
    '#description' => t('Select a default field to sort Solr listings pages by.'),
    '#options' => $options,
    '#default_value' => variable_get('apachesolr_listing_sort_default_field', ''),
  );
  $form['global']['apachesolr_listing_sort_default_direction'] = array(
    '#type' => 'select',
    '#title' => t('Field sort direction'),
    '#description' => t('The sorting direction to be used.'),
    '#options' => array('desc' => t('Descending'), 'asc' => t('Ascending')),
    '#default_value' => variable_get('apachesolr_listing_sort_default_direction', 'desc'),
  );

  return system_settings_form($form);
}
