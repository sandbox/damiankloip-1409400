<?php

/**
 * Class to deal with Modification of listing sort.
 */
class SolrListingSort {

  protected $envId, $sortField, $sortDirection, $solrData;

  //static $_instance;

  function __construct($field = NULL, $direction = NULL, $env_id = NULL) {
    $this->envId = $env_id;
    if ($field && $direction) {
      $this->setListingSort($field, $direction);
    }
    // Get SolrData object and store it in $this->solrData - already called from getListingSort.
    //$this->solrData($env_id);
    return $this;
  }
  
/*
  static function getInstance() {
    if (!isset(self::$_instance) || !instance_of(self)) {
      self::$_instance = self::__construct();
    }
    return self::$_instance;
  }
*/

  /*
   * Get Solr instance.
   * From this we can get available sort fields etc...
   */
  protected function SolrData($env_id) {
    if (empty($env_id)) {
      $env_id = apachesolr_default_environment();
    }
    try {
      $solr = apachesolr_get_solr($env_id);
      if (apachesolr_index_get_last_updated()) {
        $solr->clearCache();
      }
      $data = $solr->getLuke();
      if ($data) {
        $this->solrData = $data;
        return $data;
      }
      else {
        return NULL;
      }
    }
    catch (Exception $e) {
      watchdog('Apache Solr listing sort', nl2br(check_plain($e->getMessage())), NULL, WATCHDOG_ERROR);
      drupal_set_message(nl2br(check_plain($e->getMessage())), "warning");
    }
  }
  
  /**
   * Return SolrData from object if it's set or resuest from SolrData() method.
   * 
   * @param $flush
   *  unset SolrData variable so it is re-reqested from SolrData again.
   * 
   */
  function getSolrData($flush = FALSE) {
    if ($flush) {
      unset($this->SolrData);
    }
    return isset($this->SolrData) ? $this->SolrData : $this->SolrData($this->envId);
  }

  /**
   * Return Available fields from Solr data object.
   */
  function getSolrDataFields() {
    return isset($this->getSolrData()->fields) ? $this->getSolrData()->fields : FALSE;
  }

  /**
   * Set listing sort and validate field name/direction.
   */
  public function setListingSort($field, $direction) {
    $fields = (array) $this->getSolrDataFields();
    if (is_array($fields) && in_array($field, array_keys($fields))) {
      $this->sortField = $field;
    }
    else {
      return FALSE;
    }

    switch ($direction) {
      case 'asc':
      case 'desc':
        $this->sortDirection = $direction;
      break;
      default:
        return FALSE;
    }

    return $this;
  }
  
  public function getListingSort() {
    if (!isset($this->sortField) || !isset($this->sortDirection)) {
      return FALSE;
    }

    return (object) array(
      'field' => $this->sortField,
      'direction' => $this->sortDirection,
    );
  }
  
  /**
   * Get global defaults (if any) in same form as single path objects.
   */
  static function getGlobalDefaultSort() {
    $defaults = new stdClass();
    $defaults->field = variable_get('apachesolr_listing_sort_default_field', '');
    $defaults->direction = variable_get('apachesolr_listing_sort_default_direction', 'desc');
    return $defaults;
  }

} // SolrListingSort
